// Programa que intercanvia els valors de dues variables.

public class e14
{
    public static void main(String args[])
    {
        double x,y,aux;

        System.out.println("Introdueix un valor per a x:");
        x = Double.parseDouble(System.console().readLine());

        System.out.println("Introdueix un valor per a y:");
        y = Double.parseDouble(System.console().readLine());

        aux = x;
        x = y;
        y = aux;
        
        System.out.println("Intercanviats els valors, x ara sería " + x + ", i y sería " + y);
    }
}
