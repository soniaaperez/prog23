// Programa que accepta nombres enters majors o iguals, 0 fins a acabar amb un nombre negatiu, i indique finalment quants han sigut múltiples de 2, quants ho han sigut de 3 i quants ho són de 2 i de 3 al mateix temps (de 6 per).

public class e21
{
  public static void main(String args[])
  {
    int i, a = 0, b = 0, c = 0;


      System.out.print("Introduïx un nombre positiu (negatiu per a terminar): ");
      i = Integer.parseInt(System.console().readLine());
      while (i>=0)
      {
        if ((i%2)==0)
          a++;
        if ((i%3)==0)
          b++;
        if ((i%6)==0)
          c++;
         System.out.print("Introduïx un nombre positiu (negatiu per a terminar): ");
      	i = Integer.parseInt(System.console().readLine());
      }

    System.out.println("Aquests son divisibles per dos: " + a);
    System.out.println("Aquests son divisibles per tres: " + b);
    System.out.println("Aquests son divisibles per els dos: " + c);
  }

}
