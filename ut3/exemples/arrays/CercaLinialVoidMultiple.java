// exemple de cerca d'un valors dins d'un array amb aparicions MÚLTIPLEs: mostrant totes les aparicions

import java.util.Scanner;

public class CercaLinialVoidMultiple
{
	private static Scanner ent = new Scanner(System.in);
	
	public static void main(String args[])
	{
		final int N = 5; double valor; int posic;
		
		// creem l'array de 4 elements
		double nums[] = new double[N];
		// carregar valors des de teclat
		carregaValors(nums);
		// mostrar valors de l'array en pantalla
		mostraValors(nums);
		// demanaré un valor a cercar
		System.out.println("Introduix un valor de recerca:");	
		valor = ent.nextDouble();
		cerca(nums,valor);
	}
	
	public static void carregaValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
		{
			System.out.println("Introduix un valor numèric:");
			nums[i] = ent.nextDouble();
		}
	}
	
	public static void mostraValors(double nums[])
	{
		for (int i=0 ; i < nums.length ; i++)
			System.out.print("\tPosició " + (i + 1) + ": " + nums[i]);
		System.out.println("");
	}
	
	public static void cerca(double nums[], double v)
	{
		for (int i=0 ; i < nums.length ; i++)
			if (nums[i] == v)
				System.out.println("Trobat en la posició " +  (i + 1));
	}
}
