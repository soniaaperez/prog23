package animal;

public class elefante extends animal
{
	private double longTrompa;
	
	public elefante()
	{
		super();
		longTrompa=5;
	}
	public elefante(String c, double lt, double p)
	{
		super(p,c);
		longTrompa=lt;
	}
	
	public elefante(elefante e)
	{
		super(e.peso,e.color);
		longTrompa= e.longTrompa;
	}
	
	public double getTrompa()
	{
		return longTrompa;
	}
	public void setTrompa(double trompa)
	{
		longTrompa=trompa;
	}
	
	@Override
	public String toString()
	{
		return "Trompa= "+longTrompa+" peso= "+peso+" color= "+color;
	}
}
