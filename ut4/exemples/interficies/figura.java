// Exemple d'interfície

// típicament una interfície conté mètodes prototipats, és a dir, mètodes per implementar
public interface figura
{
	public double area();
	public double perimetre();
}
