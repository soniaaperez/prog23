package viaje;

interface Transport
{
    public double consumViatge(double km);
}