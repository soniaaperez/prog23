/* Programa que gestione un fitxer seqüencial amb dades d'empleats d'una empresa.
Cada registre contindrà identificador (enter), nom (text) i salari (real) d'un mateix treballador */

import java.io.*;
import java.util.Scanner;

public class exRegistreBinari
{
	private static Scanner ent = new Scanner(System.in);
	public static void main(String[] args) {
		
		do
		{
			System.out.println("Programa de gestió d'empleats");
			System.out.println("1. Alta d'empleat");
			System.out.println("2. Mostrar tots els empleats");
			System.out.println("3. Esborrar un empleat");
			System.out.println("0. Eixir de l'aplicació");
			System.out.println("\tTriar una opció:");
			int opcio = ent.nextInt();
			switch(opcio)
			{
				case 1: alta();break;
				case 2: mostra();break;
				case 3: esborra();break;
				case 0: System.exit(0);
				default: System.out.println("Opció inexistent\n");
			}
		} while(true);
	}

	public static void alta()
	{
		try(FileOutputStream fos = new FileOutputStream("empleats.dat",true);	// segon paràmetre a TRUE per a no perdre els empleats que ja existien
			DataOutputStream dos = new DataOutputStream(fos);)
		{
			System.out.println("Introduix número d'empleat:");
			int id = ent.nextInt();
			// per a evitar el problema del buffer del teclat
			ent.nextLine();	
			System.out.println("Introduix nom de l'empleat:");
			String nom = ent.nextLine();
			System.out.println("Introduix salari de l'empleat:");
			double salari = ent.nextDouble();
			dos.writeInt(id);
			dos.writeUTF(nom);
			dos.writeDouble(salari);
		}
		catch (IOException e)
		{
			System.err.println(e.getMessage());
		}
	}

	public static void mostra()
	{
		File f = new File("empleats.dat");
		if (f.exists())
		{
			//System.out.println("a");
			try(FileInputStream fis = new FileInputStream(f);	// segon paràmetre a TRUE per a no perdre els empleats que ja existien
				DataInputStream dis = new DataInputStream(fis);)
			{
				while (true)
				{
					int id = dis.readInt();
					String nom = dis.readUTF();
					double salari = dis.readDouble();
					System.out.println(id + "\tnom:" + nom + "\tsalari: " + salari + " euros.");
				}

			}
			catch(EOFException e)
			{

			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}

		}
		else
			System.out.println("El fitxer empleats.dat no existeix");
	}

	public static void esborra()
	{

		File f = new File("empleats.dat");
		File f2 = new File("empleats.new");
		if (f.exists())
		{
			//System.out.println("a");
			try(FileInputStream fis = new FileInputStream(f);	// segon paràmetre a TRUE per a no perdre els empleats que ja existien
				DataInputStream dis = new DataInputStream(fis);
				FileOutputStream fos = new FileOutputStream(f2);
				DataOutputStream dos = new DataOutputStream(fos);
				)
			{
				/* copiaré del fitxer de lectura al d'escriptura cada registre que no siga el que vull esborrar */
				System.out.println("¿Quin identificador d'empleat vols esborrar?:");
				int id_baixa = ent.nextInt();
				while (true)
					{
						int id = dis.readInt();
						String nom = dis.readUTF();
						double salari = dis.readDouble();
						System.out.println(id + "\tnom:" + nom + "\tsalari: " + salari + " euros.");
						if (id != id_baixa)
						{
							dos.writeInt(id); dos.writeUTF(nom);dos.writeDouble(salari);
						}
					}
			}
			catch(EOFException e)
			{
				// esborre el fitxer original
				f.delete();
				// canvie el nom del fitxer nou al original
				f2.renameTo(f);
			}
			catch(IOException e)
			{
				System.err.println(e.getMessage());
			}
		}
		else 
			System.out.println("El fitxer empleats.dat no existeix");
	}
}
