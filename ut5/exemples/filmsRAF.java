import java.io.*;
import java.util.Scanner;

class Film
{
	private static final int maxString = 100;
	// 210 Bytes és el resultat de la següent operació
	private static final int tamFilm = 2*(maxString + 1) + 2*Integer.SIZE/8;
	// Cada registre "film" constarà de codi + titol + any + director
	private int cod;
	private String title;
	private int year;
	private String director;
	
	public Film(int c, String t, int i, String d)
	{
		cod = c; year = i;
		if (t.length() > maxString)
			title = t.substring(0, maxString);
		else
			title = t;
		if (d.length() > maxString)
			director = d.substring(0, maxString);
		else
			director = d;
	}

 	public void show()
	{
		if (cod != 0)
			System.out.printf("---------------------------\n%d\t%s\n\t%d\tDirector: %s\n---------------------------\n",cod,title,year,director);
		else
			System.out.printf("Aquesta pel·lícula no existeix\n");
	}

	public void writeFile(RandomAccessFile raf)
	{
		try
		{
			// situar la marca de posició on corresponga segons el codi
			raf.seek((cod - 1)*tamFilm);
			// ara escric les dades de la pel·lícula a fitxer
			raf.writeInt(cod);
			raf.writeBytes(title + '\n');
			raf.writeInt(year);
			raf.writeBytes(director + '\n');
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void readFile(RandomAccessFile raf)
	{
		try
		{
			// situar la marca de posició on corresponga segons el codi
			raf.seek((cod-1)*tamFilm);
			// ara llig les dades de la pel·lícula
			cod=raf.readInt();
			if (cod != 0)
			{
				title=raf.readLine();
				year=raf.readInt();
				director=raf.readLine();
			}
		}
		catch (EOFException e)
		{
			System.out.printf("Aquesta pel·lícula no existeix\n");
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}	
	}
	
	public int getYear() { return year; }
}

public class filmsRAF
{
	public static void main(String[] args) throws FileNotFoundException,IOException
	{
		Scanner ent=new Scanner(System.in);
		RandomAccessFile raf=new RandomAccessFile("films.dat","rw");
		System.out.printf ( "GESTIÓ VIDEOTECA\n\t1. Introduir pel·lícula\n\t2. Consultar pel·lícula\n\t0. Eixir \nTria opció: \n");
		int opc=ent.nextInt();
		int c; Film f;
		switch (opc)
		{
			case 1: System.out.printf("Introdueix codi:");
			c = ent.nextInt ();
			// problema de la memòria intermèdia del teclat
			ent.nextLine();
			System.out.printf("Introdueix títol:");
			String t = ent.nextLine();
			System.out.printf("Introdueix any:");
			int i = ent.nextInt();
			// problema de la memòria intermèdia del teclat
			ent.nextLine();
			System.out.printf("Introdueix director:");
			String d = ent.nextLine();
			f = new Film(c, t, i, d);
			f.writeFile(raf);
			break;
			
			case 2: System.out.printf("Introdueix codi:");
			c = ent.nextInt();
			f = new Film(c,"", 0,"");
			f.readFile(raf);
			if (f.getYear() != 0)
				f.show();
			break;
			
			default: if (raf != null)
				raf.close ();
			System.out.printf( "Fi del programa");
		};

	}
}
