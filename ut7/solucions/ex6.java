/*6. Realitzar una aplicació gràfica (amb Swing) que amb un botó (etiquetatge "Següent") permeta mostrar els valors de tots els camps per a cada article de la taula corresponent. Amb cada clic ha de mostrar els valors corresponents al següent article.*/
import java.sql.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ex6
{
	//DEFINICIÓ DE LES REFERÈNCIES COM A OBJECTES.
	private static int cod=0;
	private static JFrame jf; //Finestra principal del programa.
	private static JMenuBar menuBar; //MenuBar
	private static JMenu menu; //Menu Opcions
	private static JMenuItem menuItem; //MenuItem
	private static JPanel jp1; //Primer panell del programa que contindrà un label.
	private static JPanel jp2; //Segon panell del programa que contindrà labels.
	private static JPanel jp21; //Primer panell del segon panell.
	private static JPanel jp22; //Segon panell del segon panell.
	private static JPanel jp3; //Tercer panell del programa que contindrà botons.
	private static JLabel info; //Label info
	private static JLabel id; //Label id
	private static JLabel idVal; //Label valor id
	private static JLabel nom; //Label nom
	private static JLabel nomVal; //Label valor nom
	private static JLabel preu; //Label preu
	private static JLabel preuVal; //Label valor preu
	private static JLabel codi; //Label codi
	private static JLabel codiVal; //Label codi
	private static JLabel grup; //Label grup
	private static JLabel grupVal; //Label grup
	private static JButton jb; //Botó de següent.


	public static void main(String[] args)
	{
		//FINESTRA
			//Creem la finestra.
			jf = new JFrame("Mostra Articles");
			//Definim un layout per a la finestra.
			jf.setLayout(new GridLayout(0,1));
			//Fem que el programa acabe al tancar la finestra.
			jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//Centrem la finestra.
			jf.setLocationRelativeTo(null);
			//Fem visible la finestra.
			jf.setVisible(true);

		//MENU
			//Creem els objectes del menu.
			menuBar = new JMenuBar();
			menu = new JMenu("Opcions");
			menuItem = new JMenuItem("Eixir");
			//Agreguem els objectes a la finestra.
			menu.add(menuItem);
			menuBar.add(menu);
			jf.setJMenuBar(menuBar);

		//PANELL
			//Creem el panell amb un layout.
			jp1 = new JPanel(new GridLayout(0,1));
			jp2 = new JPanel(new GridLayout(1,0));
			jp21 = new JPanel(new GridLayout(0,1));
			jp22 = new JPanel(new GridLayout(0,1));
			jp3 = new JPanel(new GridLayout(1,1));
			//Afegim els panells a la finestra.
			jf.getContentPane().add(jp1);
			jp2.add(jp21);
			jp2.add(jp22);
			jf.getContentPane().add(jp2);
			jf.getContentPane().add(jp3);

		//LABELS
			//Creem els labels.
			info = new JLabel("[Presiona el botó per a vore els articles]");
			id = new JLabel("ID");
			idVal = new JLabel("");
			nom = new JLabel("Nom");
			nomVal = new JLabel("");
			preu = new JLabel("Preu");
			preuVal = new JLabel("");
			codi = new JLabel("Codi");
			codiVal = new JLabel("");
			grup = new JLabel("Grup");
			grupVal = new JLabel("");
			//Afegim els labels als panells.
			jp1.add(info);
			jp21.add(id);
			jp21.add(nom);
			jp21.add(preu);
			jp21.add(codi);
			jp21.add(grup);
			jp22.add(idVal);
			jp22.add(nomVal);
			jp22.add(preuVal);
			jp22.add(codiVal);
			jp22.add(grupVal);

		//BOTÓ
			//Creem el botó.
			jb = new JButton("Següent");
			//Agefim el botó al segón panell.
			jp3.add(jb);

		//CONTROL D'ESDEVENIMENTS
			//Creem els ActionListeners per al boto.
			ActionListener alb = e -> {accions(e);};
			ActionListener alm = e -> {System.exit(0);};
			//Afegim els ActionListener al botó i al menuItem.
			jb.addActionListener(alb);
			menuItem.addActionListener(alm);

		//EXTRAS
			//Fem que el tamany de la finestra s'adecue al minim dels elements.
			jf.pack();


	}

	public static void accions(ActionEvent e)
	{
		String sql="SELECT a.id,a.nombre,a.precio,a.codigo,g.descripcion FROM articulos a INNER JOIN grupos g on g.id=a.grupo";
		String id,nom,preu,codi,grup;
		try(Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/empresa","root","");
			Statement stmt = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
			ResultSet rs = stmt.executeQuery(sql))
		{
			cod++;
			rs.last(); 
			int total = rs.getRow(); //Obtinc el total de línies.
			if(cod > total)
			{
				info.setText("[No n'hi han més articles]");
			}
			else
			{
				rs.absolute(cod);
				idVal.setText(rs.getString(1));
				nomVal.setText(rs.getString(2));
				preuVal.setText(rs.getString(3)+"€");
				codiVal.setText(rs.getString(4));
				grupVal.setText(rs.getString(5));
			}
			

		}
		catch(SQLException ex)
		{
			ex.printStackTrace();
		}

	}
}
